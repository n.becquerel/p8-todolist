# How to contribute
## Creating an Issue
You should always start by creating an Issue on the project's Gitlab repository

## Create a Merge Request
Once you created an Issue, you can then create a merge request from the Issue details.
Create a merge request from there will create a branch by default named by the issue name.
Choose the starting branch. This should always be starting from master except rare cases.

## Working on the project
If you work on js / css don't forget to run encore's dev script. Use the --watch option to run the assets build automatically when you modify a js / css file
```bash
yarn encore dev --watch 
```

## Before commit / push to you branch on the repository
We use different tools to guarantee good code formating / respect of good practices.  
If you can run commands from a Makefile just this command to perform analyse: 
```bash
make analyse
```
You can run tools used one like this:
```bash
composer valid \
  && composer unused \
  && ./vendor/bin/twigcs templates/ \
  && ./vendor/bin/phpcbf --standard=PSR2 src/ tests/ \
  && ./vendor/bin/phpcs --standard=PSR2 src/ tests/ \
  && ./vendor/bin/phpstan analyse -c phpstan.neon \
  && php bin/console doctrine:schema:valid --skip-sync
```
Composer valid checks for composer.json to be valid  
Composer unused checks the dependencies usage to see if one is not used by the project  
We use twigcs to check code formating for our twig templates  
Then we use php code sniffer to check our php code against PSR2 code standard. The first command (phpcbf) fixes formatting "errors" if he can. Then phpcs checks if some fixes still exists and show them us  
We use phpstan to check our php code validity like correct typehinting for example  
Then we check for the validity of our doctrine database schema  
  
  
Once you performed analysis and all went fine, don't forget to run tests. As you should always be writing tests for the features you develop.
To do so just run :
```bash
php bin/phpunit 
```
You can add a --filter tag and pass it a value to test only a certain class test or function test.

## Commit you work to the repostory
In your commit message please add in the #number of the issue at the beginning. This helps keep track of code in the commit.
For example: 
```bash
git commit -m "#34 resolve some bug and add some feature"
```
Push your commit to your branch on the repository.  
A pipeline doing same sorts of actions on the repository will perform analyse and run tests.  
Once the pipeline passes, you can mark the merge request as "ready" and then if approved, you can automatically merge to master on the merge request page.

