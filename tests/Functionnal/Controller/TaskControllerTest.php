<?php

namespace App\Tests\Functionnal\Controller;

use App\Entity\Task;
use App\Tests\Traits\UserTrait;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;
    use UserTrait;

    public function testIndex()
    {
        $client = self::createClient();
        $client->request('GET', '/tasks');
        $this->assertResponseRedirects();

        $client->loginUser($this->getAdmin())->request('GET', '/tasks');
        $this->assertResponseIsSuccessful();

        $crawler = $client->loginUser($this->getUser())->request('GET', '/tasks');
        $this->assertResponseIsSuccessful();
        $this->assertCount(15, $crawler->filter('.todo'));

        $anonymousTodo = [];
        $user2Todo = [];
        foreach ($crawler->filter('.todo-author') as $todoAuthor) {
            if ($todoAuthor->textContent === 'Par: Anonyme') {
                $anonymousTodo[] = '';
            } elseif ($todoAuthor->textContent === 'Par: user') {
                $user2Todo[] = '';
            }
        }
        $this->assertCount(5, $anonymousTodo);
        $this->assertCount(5, $user2Todo);
    }

    public function testCreateTaskNeedAuthenticatedUser()
    {
        $client = self::createClient();
        $client->request('GET', '/tasks/create');
        $this->assertResponseRedirects();

        $client->loginUser($this->getUser())->request('GET', '/tasks/create');
        $this->assertResponseIsSuccessful();

        $client->loginUser($this->getAdmin())->request('GET', '/tasks/create');
        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider provideTasks
     */
    public function testCreateTask($data)
    {
        $client = self::createClient()->loginUser($this->getUser());
        $client->request('GET', '/tasks/create');
        $client->submitForm('Ajouter', $data['task']);

        if (isset($data['errors'])) {
            $this->assertResponseIsSuccessful();

            foreach ($data['errors'] as $error) {
                $this->assertStringContainsString($error, $client->getResponse()->getContent());
            }
        } else {
            $this->assertResponseRedirects('/tasks');

            $client->followRedirect();
            $this->assertStringContainsString(
                'La tâche a été bien été ajoutée.',
                $client->getResponse()->getContent()
            );
        }
    }

    public function testCreateTaskHasAuthor()
    {
        $client = self::createClient();
        $user = $this->getUser();
        $client->loginUser($user);
        $client->request('GET', '/tasks/create');

        $client->submitForm('Ajouter', [
            'task[title]' => '__tctha__',
            'task[content]' => '__tctha__'
        ]);

        $client->followRedirect();

        $createdTask = static::getContainer()->get('doctrine')->getRepository(Task::class)
            ->findOneBy(['title' => '__tctha__']);

        $this->assertSame($createdTask->getAuthor()->getId(), $user->getId());
    }

    public function provideTasks(): array
    {
        return [
            [
                [
                    'task' => [
                        'task[title]' => 'test',
                        'task[content]' => 'test'
                    ]
                ],
            ],
            [
                [
                    'task' => [
                        'task[title]' => '',
                        'task[content]' => 'Test'
                    ],
                    'errors' => [
                        'Vous devez saisir un titre.'
                    ]
                ],
            ],
            [
                [
                    'task' => [
                        'task[title]' => 'Test',
                        'task[content]' => ''
                    ],
                    'errors' => [
                        'Vous devez saisir du contenu.'
                    ]
                ],
            ],
            [
                [
                    'task' => [
                        'task[title]' => '',
                        'task[content]' => ''
                    ],
                    'errors' => [
                        'Vous devez saisir un titre.',
                        'Vous devez saisir du contenu.'
                    ]
                ]
            ],
            [
                [
                    'task' => [
                        'task[title]' => '',
                        'task[content]' => ''
                    ],
                    'errors' => [
                        'Vous devez saisir un titre.',
                        'Vous devez saisir du contenu.'
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider provideTasks
     */
    public function testEditTask(array $data)
    {
        $client = self::createClient()->loginUser($this->getUser());
        $client->request('GET', '/tasks/1/edit');
        $client->submitForm('Modifier', $data['task']);

        if (isset($data['errors'])) {
            $this->assertResponseIsSuccessful();

            foreach ($data['errors'] as $error) {
                $this->assertStringContainsString($error, $client->getResponse()->getContent());
            }
        } else {
            $this->assertResponseRedirects('/tasks');

            $client->followRedirect();
            $this->assertStringContainsString(
                'La tâche a bien été modifiée.',
                $client->getResponse()->getContent()
            );
        }
    }

    public function testToggleTaskAction()
    {
        $client = self::createClient();
        $client->request('GET', '/tasks/1/toggle');
        $this->assertResponseRedirects();

        $client->loginUser($this->getUser())->request('GET', '/tasks/1/toggle');
        $this->assertResponseRedirects('/tasks');
        $client->followRedirect();
        $this->assertStringContainsString('a bien été marquée comme faite.', $client->getResponse()->getContent());
    }

    public function testDeleteTaskAction()
    {
        $client = self::createClient();
        $client->loginUser($this->getAdmin())->request('GET', '/tasks/1/delete');
        $this->assertResponseRedirects('/tasks');
        $client->followRedirect();
        $this->assertStringContainsString('La tâche a bien été supprimée.', $client->getResponse()->getContent());

        $client->loginUser($this->getUser())->request('GET', '/tasks/6/delete');
        $this->assertResponseRedirects('/tasks');
        $client->followRedirect();
        $this->assertStringContainsString('La tâche a bien été supprimée.', $client->getResponse()->getContent());
    }

    public function testDeleteTaskRequireAuth()
    {
        $client = self::createClient();
        $client->request('GET', '/tasks/1/delete');
        $this->assertResponseRedirects();
    }

    public function testDeleteTaskRequireOwningUser()
    {
        $client = self::createClient();
        $client->loginUser($this->getUser())->request('GET', '/tasks/11/delete');
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertStringContainsString(
            'Vous n&#039;avez pas les droits suffisants pour supprimer cette tâche.',
            $client->getResponse()->getContent()
        );
    }

    public function testDeleteAnonymousTaskByAdminOnly()
    {
        $client = self::createClient();
        $client->loginUser($this->getUser())->request('GET', '/tasks/1/delete');
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertStringContainsString(
            'Vous n&#039;avez pas les droits suffisants pour supprimer cette tâche.',
            $client->getResponse()->getContent()
        );


        $client->loginUser($this->getAdmin())->request('GET', '/tasks/1/delete');
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertStringContainsString(
            'La tâche a bien été supprimée.',
            $client->getResponse()->getContent()
        );
    }
}
