<?php

namespace App\Tests\Functionnal\Controller;

use App\Tests\Traits\UserTrait;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;
    use UserTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
    }

    public function testListAction()
    {
        $this->client->request('GET', '/users');
        $this->assertResponseRedirects();

        $this->client->loginUser($this->getUser())->request('GET', '/users');
        $this->assertResponseStatusCodeSame(403);

        $this->client->loginUser($this->getAdmin())->request('GET', '/users');
        $this->assertResponseIsSuccessful();
    }

    public function testCreateActionPageIsUp()
    {
        $this->client->request('GET', '/users/create');
        $this->assertResponseRedirects();

        $this->client->loginUser($this->getUser())->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(403);

        $this->client->loginUser($this->getAdmin())->request('GET', '/users/create');
        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider provideUser
     */
    public function testCreateAction($data)
    {
        $client = $this->client->loginUser($this->getAdmin());
        $client->request('GET', '/users/create');
        $client->submitForm('Ajouter', $data['user']);

        if (isset($data['errors'])) {
            $this->assertResponseIsSuccessful();

            foreach ($data['errors'] as $error) {
                $this->assertStringContainsString($error, $client->getResponse()->getContent());
            }
        } else {
            $this->assertResponseRedirects('/users');

            $client->followRedirect();
            $this->assertStringContainsString(
                'L&#039;utilisateur a bien été ajouté.',
                $client->getResponse()->getContent()
            );
        }
    }

    public function provideUser(): array
    {
        return [
            [
                [
                    'user' => [
                        'user[username]' => 'username',
                        'user[password][first]' => 'password',
                        'user[password][second]' => 'password',
                        'user[email]' => 'test@test.com',
                        'user[roles]' => [
                            'ROLE_USER',
                            'ROLE_ADMIN'
                        ],
                    ],
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => '',
                        'user[password][first]' => 'password',
                        'user[password][second]' => 'password',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Vous devez saisir un nom d&#039;utilisateur.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username',
                        'user[password][first]' => 'test1',
                        'user[password][second]' => 'test',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Les deux mots de passe doivent correspondre.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test1',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Les deux mots de passe doivent correspondre.'
                    ]
                ], // TODO retirer la validation html et ajouter un message en cas de mdp vide
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test',
                        'user[email]' => '',
                    ],
                    'errors' => [
                        'Vous devez saisir une adresse email.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'test',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test',
                        'user[email]' => 'test',
                    ],
                    'errors' => [
                        'Le format de l&#039;adresse n&#039;est pas correcte.'
                    ]
                ],
            ]
        ];
    }

    /**
     * @dataProvider provideUserEdit
     */
    public function testEditAction($data)
    {
        $client = $this->client->loginUser($this->getAdmin());
        $client->request('GET', '/users/3/edit');
        $client->submitForm('Modifier', $data['user']);

        if (isset($data['errors'])) {
            $this->assertResponseIsSuccessful();

            foreach ($data['errors'] as $error) {
                $this->assertStringContainsString($error, $client->getResponse()->getContent());
            }
        } else {
            $this->assertResponseRedirects('/users');

            $client->followRedirect();
            $this->assertStringContainsString(
                'L&#039;utilisateur a bien été modifié',
                $client->getResponse()->getContent()
            );
        }
    }

    public function provideUserEdit(): array
    {
        return [
            [
                [
                    'user' => [
                        'user[username]' => 'username1',
                        'user[password][first]' => 'password',
                        'user[password][second]' => 'password',
                        'user[email]' => 'test2@test.com',
                        'user[roles]' => [
                            'ROLE_ADMIN'
                        ]
                    ],
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => '',
                        'user[password][first]' => 'password',
                        'user[password][second]' => 'password',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Vous devez saisir un nom d&#039;utilisateur.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username1',
                        'user[password][first]' => 'test1',
                        'user[password][second]' => 'test',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Les deux mots de passe doivent correspondre.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username1',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test1',
                        'user[email]' => 'test@test.com',
                    ],
                    'errors' => [
                        'Les deux mots de passe doivent correspondre.'
                    ]
                ], // TODO retirer la validation html et ajouter un message en cas de mdp vide
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username1',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test',
                        'user[email]' => '',
                    ],
                    'errors' => [
                        'Vous devez saisir une adresse email.'
                    ]
                ],
            ],
            [
                [
                    'user' => [
                        'user[username]' => 'username1',
                        'user[password][first]' => 'test',
                        'user[password][second]' => 'test',
                        'user[email]' => 'test',
                    ],
                    'errors' => [
                        'Le format de l&#039;adresse n&#039;est pas correcte.'
                    ]
                ],
            ]
        ];
    }
}
