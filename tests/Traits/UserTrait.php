<?php

namespace App\Tests\Traits;

use App\Entity\User;
use App\Repository\UserRepository;

trait UserTrait
{
    public function getAdmin(): User
    {
        return $this->findUserByUsername('admin');
    }

    public function getUser(): User
    {
        return $this->findUserByUsername('user');
    }

    public function findUserByUsername(string $username): User
    {
        return static::getContainer()->get(UserRepository::class)->findOneBy(['username' => $username]);
    }
}
