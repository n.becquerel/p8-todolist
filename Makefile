db:
	php bin/console d:d:d -f --if-exists
	php bin/console d:d:c -n
	php bin/console d:m:m -n
	php bin/console hautelook:fixtures:load -n
db-test:
	php bin/console d:d:d -f --if-exists -e test
	php bin/console d:d:c -n -e test
	php bin/console d:m:m -n -e test
	php bin/console hautelook:fixtures:load -n -e test

analyse:
	composer valid
	composer unused
	./vendor/bin/twigcs templates/
	./vendor/bin/phpcbf --standard=PSR2 src/ tests/
	./vendor/bin/phpcs --standard=PSR2 src/ tests/
	./vendor/bin/phpstan analyse -c phpstan.neon
	php bin/console doctrine:schema:valid --skip-sync

optimize:
	composer dump-autoload  --classmap-authoritative
