# TodoList

[![pipeline status](https://gitlab.com/n.becquerel/p8-todolist/badges/master/pipeline.svg)](https://gitlab.com/n.becquerel/p8-todolist/-/commits/master)
[![coverage report](https://gitlab.com/n.becquerel/p8-todolist/badges/master/coverage.svg)](https://gitlab.com/n.becquerel/p8-todolist/-/commits/master)

## Installation guide

## Dependencies installation
Once project is cloned etc... Install php dependencies via composer
```bash
composer install
```
This project also use Webpack Encore by Symfony. You will need to install js dependencies
```bash
# With Npm
npm install
# With Yarn
yarn install
```

### Setting up database
If you can run Makefiles commands, just run :
```bash
make db
# A command also exists for the test env
make db-test
```

First you need to create database. Run :
``` bash
php bin/console doctrine:database:create
```
Then load schema with migrations:
``` bash 
php bin/console doctrine:migration:migrate -n
```
Then load initial dataset:
``` bash 
php bin/console hautelook:fixtures:load -n
```

### Contributing
[See how to contribute](CONTRIBUTING.md)
