<?php

namespace App\Form\Maker;

use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormMaker
{
    /**
     * @var FormInterface<FormType>|null
     */
    private ?FormInterface $form;
    private ServiceLocator $container;
    private FormFactoryInterface $formFactory;
    private object $entity;

    public function __construct(ServiceLocator $container, FormFactoryInterface $formFactory)
    {
        $this->container = $container;
        $this->formFactory = $formFactory;
    }

    /**
     * @param string $formType
     * @param object $entity
     * @param array<string, mixed> $formOptions
     * @return FormInterface<FormType>|null
     */
    public function makeForm(string $formType, object $entity, array $formOptions = []): ?FormInterface
    {
        $this->entity = $entity;

        $this->form = $this->formFactory->create($formType, $entity, $formOptions);

        return $this->form;
    }

    public function handle(Request $request, string $handlerClassName): ?bool
    {
        $this->form->handleRequest($request);

        if ($this->checkIfFormIsValid()) {
            $this->handleSuccess($request, $this->entity, $handlerClassName);
            return true;
        }

        return null;
    }

    private function checkIfFormIsValid(): bool
    {
        return $this->form->isSubmitted() && $this->form->isValid();
    }

    private function handleSuccess(Request $request, object $entity, string $handlerClassName): void
    {
        $this->container->get($handlerClassName)->withRequest($request)->handle($entity);
    }
}
