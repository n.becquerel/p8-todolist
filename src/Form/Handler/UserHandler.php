<?php

namespace App\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserHandler extends AbstractFormHandler
{
    private UserPasswordHasherInterface $passwordHasher;
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager, UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
        $this->manager = $manager;
    }

    public function handle(object $entity): void
    {
        if (!$entity->getId()) {
            $this->handleNew($entity);
            return;
        }

        $this->handleEdit($entity);
    }

    private function handleNew(object $entity): void
    {
        $hashedPassword = $this->passwordHasher->hashPassword($entity, $entity->getPassword());
        $entity->setPassword($hashedPassword);

        $this->manager->persist($entity);
        $this->manager->flush();
    }

    private function handleEdit(object $entity): void
    {
        $hashedPassword = $this->passwordHasher->hashPassword($entity, $entity->getPassword());
        $entity->setPassword($hashedPassword);

        $this->manager->flush();
    }
}
