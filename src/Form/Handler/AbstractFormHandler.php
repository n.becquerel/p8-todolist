<?php

namespace App\Form\Handler;

use Symfony\Component\HttpFoundation\Request;

abstract class AbstractFormHandler implements FormHandlerInterface
{
    protected object $entity;
    protected Request $request;

    abstract public function handle(object $entity): void;

    public function withRequest(Request $request): self
    {
        $this->request = $request;
        return $this;
    }
}
