<?php

namespace App\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;

class TaskHandler extends AbstractFormHandler
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function handle(object $entity): void
    {
        if (!$entity->getId()) {
            $this->handleNew($entity);
            return;
        }
        
        $this->handleEdit();
    }

    private function handleNew(object $entity): void
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }

    private function handleEdit(): void
    {
        $this->manager->flush();
    }
}
