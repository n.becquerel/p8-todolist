<?php

namespace App\EventListener;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class TaskListener
{
    private Security $security;
    private SessionInterface $session;

    /**
     * TaskSubscriber constructor.
     * @param Security $security
     * @param SessionInterface $session
     */
    public function __construct(Security $security, SessionInterface $session)
    {
        $this->security = $security;
        $this->session = $session;
    }

    public function prePersist(Task $task, LifecycleEventArgs $lifecycleEventArgs): void
    {
        if ($task->getAuthor()) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        $task->setAuthor($user);
    }

    public function preRemove(Task $task, LifecycleEventArgs $lifecycleEventArgs): void
    {
        $user = $this->security->getUser();

        if ((!$user instanceof User) ||
            ($task->getAuthor() && $task->getAuthor()->getId() !== $user->getId()) ||
            (!$task->getAuthor() && !$this->security->isGranted('ROLE_ADMIN'))
        ) {
            /** @phpstan-ignore-next-line */
            $this->session->getFlashBag()->add(
                'error',
                'Vous n\'avez pas les droits suffisants pour supprimer cette tâche.'
            );
            $lifecycleEventArgs->getObjectManager()->clear(Task::class);

            return;
        }

        /** @phpstan-ignore-next-line */
        $this->session->getFlashBag()->add('success', 'La tâche a bien été supprimée.');
    }
}
